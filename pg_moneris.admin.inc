<?php

/*
 * Menu callback
 */
function pg_moneris_payform($form_state,$t) {
  global $user;
  if($user->uid!=1 && $user->uid != $t->uid){
    drupal_access_denied();
  }

  drupal_set_title($t->title);
  $form['description'] = array('#value' => '<p>'.$t->description.'</p>');
  
  $form['amount'] = array('#value' => t('Total: !amount', array('!amount' => theme('pgapi_format_price', $t->amount,'CAD $',TRUE))));
  
  $form['charge_total'] = array(
     '#type' => 'hidden',
     '#value' => $t->amount,
   );
  
  $form['order_id'] = array(
    '#type' => 'hidden',
    '#value' => $t->txnid . '.'. time() . strlen($t->txnid),
  );
  
  $form['rvartxnid'] = array(
    '#type' => 'hidden',
    '#value' => $t->txnid,
  );
  
  $form['cust_id'] = array(
    '#type' => 'hidden',
    '#value' => $t->uid,
  );
  
  $form['email'] = array(
    '#type' => 'hidden',
    '#value' => $user->mail,
  );
  
  $form['lang'] = array(
    '#type' => 'hidden',
    '#value' => variable_get('pg_moneris_lang', 'en-ca'),
  );

  $form['ps_store_id'] = array(
    '#type' => 'hidden',
    '#default_value' => variable_get('pg_moneris_ps_store_id', ''),
  );

  $form['hpp_key'] = array(
    '#type' => 'hidden',
    '#default_value' => variable_get('pg_moneris_hpp_key', ''),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Click to proceed to Secure Payment Page'),
  );
  
  //allow for local tests that bypass moneris
  if(variable_get('pg_moneris_action_url', '') == 'test') {
    $form['#action'] = url('moneris/simulate/action', array('absolute' => TRUE));
  } else {
    $form['#action'] = variable_get('pg_moneris_action_url', 'https://esqa.moneris.com/HPPDP/index.php');
  }
  return $form;
}

/*
 * Menu callback
 * callback url for moneris
 */
function pg_moneris_payment_end($type) {
  global $user;
  // store all the data moneris passed us
  $txnid = substr($_POST['response_order_id'],0,strpos($_POST['response_order_id'],'.'));
  $message = $_POST['message'];
  $t=pgapi_transaction_load($txnid);
  $t->extra['response'] = $_POST;
  watchdog('paymentgateway', 'Moneris said "!status" for transaction !txnid.', array('!txnid' => $txnid, '!status' => $message), WATCHDOG_NOTICE);
  switch($type) {
    default:
    case 'fail':
      $t->status = pgapi_get_status_id('denied');
      drupal_set_message(t('Your payment did not complete successfully. Please try again'), 'error');
      //save the updated transaction
      pgapi_transaction_save($t);
      if($t->uid == $user->uid){
        $goto = pgapi_GetBackUrl($t,$type);
        drupal_goto($goto);
      }
      break;
    case 'success':      
      // verify the transaction id
      $transactionKey = $_POST['transactionKey'];
      $hpp_key = variable_get('pg_moneris_hpp_key','');
      $ps_store_id = variable_get('pg_moneris_ps_store_id','');
      //save the updated transaction
      pgapi_transaction_save($t);
        //allow for local tests that bypass moneris
      if(variable_get('pg_moneris_action_url', '') == 'test') {
        drupal_goto('moneris/simulate/verify', "ps_store_id=$ps_store_id&hpp_key=$hpp_key&transactionKey=$transactionKey");
      } else {
        drupal_goto(variable_get('pg_moneris_verification_url','https://esqa.moneris.com/HPPDP/verifyTxn.php'), "ps_store_id=$ps_store_id&hpp_key=$hpp_key&transactionKey=$transactionKey");
      }
      break;
  }
}

/*
 * Menu callback
 * verification callback for moneris
 */
function pg_moneris_payment_verify() {
  global $user;
  $txnid = substr($_POST['order_id'],0,strpos($_POST['order_id'],'.'));
  $t=pgapi_transaction_load($txnid);
  $type = 'fail';
  if($_POST['status'] == 'Valid-Approved') {
    $type = 'success';
    $t->status = pgapi_get_status_id('completed');
    $t->extra['verify'] = $_POST;
    watchdog('paymentgateway', 'Moneris verified transaction !txnid.', array('!txnid' => $t->status), WATCHDOG_NOTICE);
  } else {
    //A transaction verification failed
    drupal_set_message(t('An error occured while verifying your transaction. Please contact support. Transaction id %txnid', array('%txnid' => $txnid)), 'error');
    $t->status = pgapi_get_status_id('failed');
    $t->extra['verify'] = $_POST;
    watchdog('paymentgateway', 'Moneris did not verify transaction !txnid.', array('!txnid' => $t->status), WATCHDOG_CRITICAL);
  }
  pgapi_transaction_save($t);
  if($t->uid == $user->uid){
    $goto = pgapi_GetBackUrl($t,$type);
    drupal_goto($goto);
  }
}

function pg_moneris_settings() {
  $form['pg_moneris_action_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Hosted Paypage URL'),
    '#default_value' => variable_get('pg_moneris_action_url','https://esqa.moneris.com/HPPDP/index.php'),
    '#description'   => t("dev: %dev<br/>prod: %prod<br/>Note: set to 'test' if you'd like to simulate moneris responses locally", array('%dev' => 'https://esqa.moneris.com/HPPDP/index.php', '%prod' => 'https://www3.moneris.com/HPPDP/index.php')),
    '#required'      => TRUE
  );
  $form['pg_moneris_ps_store_id'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Store ID'),
    '#default_value' => variable_get('pg_moneris_ps_store_id',''),
    '#description'   => t("Please enter your Moneris ps_store_id."),
    '#required'      => TRUE
  );
  $form['pg_moneris_hpp_key'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Security Key'),
    '#default_value' => variable_get('pg_moneris_hpp_key',''),
    '#description'   => t("Please enter your Moneris pg_moneris_hpp_key."),
    '#required'      => TRUE
  );
  $form['pg_moneris_lang'] = array(
    '#type'          => 'select',
    '#title'         => t('Language'),
    '#options'       => array('en-ca', 'fr-ca'),
    '#default_value' => variable_get('pg_moneris_lang','en-ca'),
    '#description'   => t("The language for the Hosted Paypage."),
    '#required'      => TRUE
  );
  $form['pg_moneris_verification_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Transaction Verification URL'),
    '#default_value' => variable_get('pg_moneris_verification_url','https://esqa.moneris.com/HPPDP/verifyTxn.php'),
    '#description'   => t("dev: %dev<br/>prod: %prod<br/>Note: if test is set for Paypage URL above, this setting is ignored.", array('%dev' => 'https://esqa.moneris.com/HPPDP/verifyTxn.php', '%prod' => 'https://www3.moneris.com/HPPDP/verifyTxn.php')),
    '#required'      => TRUE
  );
  $form['pg_moneris_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Moneris Settings'),
    '#description' => t('Please use the following settings in your Moneris Hosted Paypage config')
  );
  $form['pg_moneris_info']['pg_moneris_response_method'] = array(
    '#type'          => 'markup',
    '#value'         => '<p>'.t('Response Method: POST (_not_ XML)').'</p>',
  );
  $form['pg_moneris_info']['pg_moneris_approved_url'] = array(
    '#type'          => 'markup',
    '#value'         => '<p>'.t('Approved URL: ').url('moneris/success',array('absolute' => TRUE)).'</p>',
  );
  $form['pg_moneris_info']['pg_moneris_declined_url'] = array(
    '#type'          => 'markup',
    '#value'         => '<p>'.t('Declined URL: ').url('moneris/fail',array('absolute' => TRUE)).'</p>',
  );
  $form['pg_moneris_info']['pg_moneris_transaction_verification'] = array(
    '#type'          => 'markup',
    '#value'         => '<p>'.t('Transaction Verification: ON').'</p>',
  );
  $form['pg_moneris_info']['pg_moneris_transaction_verification_method'] = array(
    '#type'          => 'markup',
    '#value'         => '<p>'.t('Transaction Verification Response Method: POST').'</p>',
  );
  $form['pg_moneris_info']['pg_moneris_transaction_verification_url'] = array(
    '#type'          => 'markup',
    '#value'         => '<p>'.t('Transaction Verification Response URL: ').url('moneris/verify',array('absolute' => TRUE)).'</p>',
  );

  return system_settings_form($form);
}


/*
 * Menu Callback
 * This is used if you want to simulate a success response from moneris, without actually hitting them
 */
function pg_moneris_payment_simulate_action($form_state) {
  $form['response_order_id'] = array(
    '#type' => 'hidden',
    '#value' => $_POST['order_id'],
  );
  $form['transactionKey'] = array(
    '#type' => 'hidden',
    '#value' => $_POST['order_id'],
  );
  $form['message'] = array(
    '#type' => 'hidden',
    '#value' => 'Local Test Transaction',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Click to simulate a successful transaction'),
  );
  $form['#action'] = url('moneris/success',array('absolute' => TRUE));
  return $form;
}

/*
 * Menu Callback form
 * This is used if you want to simulate a success response from moneris, without actually hitting them
 */
function pg_moneris_payment_simulate_verify($form_state) {
  $form['order_id'] = array(
     '#type' => 'hidden',
     '#value' => $_GET['transactionKey'],
   );
  $form['status'] = array(
    '#type' => 'hidden',
    '#value' => 'Valid-Approved',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Click to simulate a successful verification'),
  );
  $form['#action'] = url('moneris/verify',array('absolute' => TRUE));
  return $form;
}
