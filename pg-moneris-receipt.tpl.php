<div id="monerisreceipt">
<p><?php print $extra_html ?></p>
<h3><?php print t('Payment Details'); ?></h3>
<dl>
<dt><?php print t('Cardholder Name') ?></dt><dd><?php print $response_arr['cardholder'] ?></dd>
<dt><?php print t('Transaction Type') ?></dt><dd><?php print $response_arr['trans_name'] ?></dd>
<dt><?php print t('Date / Time') ?></dt><dd><?php print $response_arr['date_stamp'] . ' ' . $response_arr['time_stamp'] ?></dd>
<dt><?php print t('Transaction Amount') ?></dt><dd><?php print $formatted_price ?></dd>
<dt><?php print t('Order ID') ?></dt><dd><?php print $response_arr['response_order_id'] ?></dd>
</dl>

<dl>
<dt><?php print t('Authorization Code') ?></dt><dd><?php print $response_arr['bank_approval_code'] ?></dd>
<dt><?php print t('Response Code') ?></dt><dd><?php print $response_arr['response_code'] ?></dd>
<dt><?php print t('ISO Code') ?></dt><dd><?php print $response_arr['iso_code'] ?></dd>
<dt><?php print t('Response Message') ?></dt><dd><?php print $response_arr['message'] ?></dd>
<dt><?php print t('Reference Number') ?></dt><dd><?php print $response_arr['bank_transaction_id'] ?></dd>
</dl>

<dl>
<dt><?php print t('Product') ?></dt><dd><?php print $goods_html ?></dd>
</dl>

<dl>
<dt><?php print t('Merchant') ?></dt><dd><?php print $merchant ?></dd>
<dt><?php print t('Merchant URL') ?></dt><dd><?php print $site_url ?></dd>
<dt><?php print t('Refund Policy') ?></dt><dd><?php print $refund_link?></dd>
</dl>

</div>